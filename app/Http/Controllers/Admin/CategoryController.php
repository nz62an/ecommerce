<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->paginate(20);

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentCategories = Category::where('parent_id', 0)->get();
        $attributes = Attribute::all();

        return view('admin.categories.create', compact('parentCategories', 'attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required|unique:categories,slug',
            'parent_id' => 'required',
            'attribute_ids' => 'exclude_if:parent_id,0|required',
            'attribute_is_filter_ids' => 'exclude_if:parent_id,0|required',
            'variation_id' => 'exclude_if:parent_id,0|required'
        ]);

        try {
            DB::beginTransaction();

            $category = Category::create([
                'name' => $request->name,
                'parent_id' => $request->parent_id,
                'slug' => $request->slug,
                'is_active' => $request->is_active,
                'icon' => $request->icon,
                'description' => $request->description

            ]);

            if (isset($request->attribute_ids)) {
                foreach ($request->attribute_ids as  $attribute_id) {
                    $attribute = Attribute::findOrFail($attribute_id);
                    $attribute->categories()->attach($category->id, [
                        'is_filter' => in_array($attribute_id, $request->attribute_is_filter_ids),
                        'is_variation' => $attribute_id == $request->variation_id ? true : false
                    ]);
                }
            }

            Db::commit();
        } catch (\Exception $exception) {
            Db::rollBack();
            alert()->error('خطایی در ذخیره دسته بندی رخ داده است', $exception->getMessage())->persistent('حله');
            return redirect()->back();
        }

        alert()->success('دسته بندی مورد نظر با موفقیت اضافه شد', 'با تشکر');
        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parentCategories = Category::where('parent_id', 0)->get();
        $attributes = Attribute::all();
        return view('admin.categories.edit', compact('category', 'parentCategories', 'attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {

        $request->validate([
            'name' => 'required',
            'slug' => 'required|unique:categories,slug,' . $category->id,
            'parent_id' => 'required',
            'attribute_ids' => 'exclude_if:parent_id,0|required',
            'attribute_is_filter_ids' => 'exclude_if:parent_id,0|required',
            'variation_id' => 'exclude_if:parent_id,0|required'
        ]);
        try {
            DB::beginTransaction();

            $category->update([
                'name' => $request->name,
                'slug' => $request->slug,
                'parent_id' => $request->parent_id,
                'is_active' => $request->is_active,
                'icon' => $request->icon,
                'description' => $request->description
            ]);

            $category->attributes()->syncWithPivotValues($request->attribute_ids, ['is_filter' => false, 'is_variation' => false]);

            foreach ($request->attribute_is_filter_ids as $attribute_is_filter_id) {
                $category->attributes()->updateExistingPivot($attribute_is_filter_id, ['is_filter' => true]);
            }

            $category->attributes()->updateExistingPivot($request->variation_id, ['is_variation' => true]);

            Db::commit();
        } catch (\Exception $exception) {
            Db::rollBack();
            alert()->error('خطایی در ویراش دسته بندی رخ داده است', $exception->getMessage())->persistent('حله');
            return redirect()->back();
        }

        alert()->success('دسته بندی مورد نظر با موفقیت ویراش شد', 'با تشکر');
        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    public function getCategoryAttributes(Category $category)
    {
        $filterableAttributes = $category->attributes()->wherePivot('is_variation', 0)->get();
        $variationAttribute = $category->attributes()->wherePivot('is_variation', 1)->first();

        return [
            'attributes' => $filterableAttributes,
            'variation' => $variationAttribute
        ];
    }
}
