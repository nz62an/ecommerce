<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $guarded = [];

    /**
     * The attribues that belongs to category.
     *
     * @return void
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    /**
     * The parent category
     *
     * @return void
     */
    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }

    public function getIsActiveAttribute($value){
        return $value ? 'فعال':'غیرفعال';
    }
}
