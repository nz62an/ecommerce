@extends('admin.layouts.admin')

@section('title')
    Edit Attribute
@endsection

@section('content')

    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">ویرایش ویژگی </h5>
            </div>
            <hr>
            @include('admin.sections.errors')
            <form action="{{ route('admin.attributes.update', ['attribute' => $attribute->id]) }}" method="post">
                @csrf
                @method('put')
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">نام</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ $attribute->name }}">
                    </div>
                </div>

                <button class="btn btn-outline-success mt-5" type="submit">
                    <i class="fa fa-fw fa-save"></i> تأیید</button>
                <a class="btn btn-dark mt-5 mr-3" href="{{ route('admin.attributes.index') }}"><i class="fa fa-fw fa-chevron-left"></i>بازگشت</a>
            </form>

        </div>
    </div>
@endsection
