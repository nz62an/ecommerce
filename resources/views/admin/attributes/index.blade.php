@extends('admin.layouts.admin')

@section('title')
    Index Attribute
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="d-flex mb-5 justify-content-between">
                <h5 class="font-weight-bold">لیست ویژگی ها ({{ $attributes->total() }})</h5>
                <a class="btn btn-outline-primary" href="{{ route('admin.attributes.create') }}">
                    <i class="fa fa-plus"></i>
                    ایجاد ویژگی
                </a>
            </div>
            <hr>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ردیف</th>
                        <th scope="col">نام ویژگی</th>
                        <th scope="col">عملیات</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($attributes as $key => $attribute)
                        <tr>
                            <th scope="row">{{ $attributes->firstItem() + $key }}</th>
                            <td>{{ $attribute->name }}</td>
                            <td class="col-md-auto">
                                <a class="btn btn-outline-primary" href="{{ route('admin.attributes.show', ['attribute' => $attribute->id]) }}">
                                    <li class="fa fa-eye"></li>
                                    نمایش
                                </a>
                                <a class="btn btn-outline-success" href="{{ route('admin.attributes.edit', ['attribute' => $attribute->id]) }}">
                                    <i class="fa fa-edit"></i>
                                    ویرایش
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $attributes->links() }}
        </div>
    </div>
@endsection
