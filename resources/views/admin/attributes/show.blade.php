@extends('admin.layouts.admin')

@section('title')
    Show Attribute
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">نمایش ویژگی</h5>
            </div>
            <hr>

            <div class="row">
                <div class="form-group col-md-3">
                    <label>نام</label>
                    <input class="form-control" type="text" disabled value="{{ $attribute->name }}">
                </div>
                <div class="form-group col-md-3">
                    <label>تاریخ ایجاد</label>
                    <input class="form-control" type="text" disabled value="{{ verta($attribute->created_at) }}">
                </div>
            </div>

            <a class="btn btn-dark mt-5" href="{{ route('admin.attributes.index') }}">بازگشت</a>

        </div>
    </div>
@endsection
