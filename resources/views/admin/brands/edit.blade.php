@extends('admin.layouts.admin')

@section('title')
    Edit Brand
@endsection

@section('content')

    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">ویرایش برند <strong class="text-warning">{{$brand->name}}</strong></h5>
            </div>
            <hr>
            @include('admin.sections.errors')
            <form action="{{ route('admin.brands.update', ['brand' => $brand->id]) }}" method="post">
                @csrf
                @method('put')
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">نام</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ $brand->name }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="is_active">وضعیت</label>
                        <select class="form-control" name="is_active" id="is_active">
                            <option @if ($brand->is_active == 'غیرفعال') {{ 'selected' }} @endif value="0">غیرفعال</option>
                            <option @if ($brand->is_active == 'فعال') {{ 'selected' }} @endif value="1">فعال</option>
                        </select>
                    </div>
                </div>

                <button class="btn btn-outline-success mt-5" type="submit"> <i class="fa fa-fw fa-save"></i>تأیید</button>
                <a class="btn btn-dark mt-5 mr-3" href="{{ route('admin.brands.index') }}"><i class="fa fa-fw fa-chevron-left"></i> بازگشت</a>
            </form>

        </div>
    </div>
@endsection
