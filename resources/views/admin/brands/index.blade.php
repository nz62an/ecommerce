@extends('admin.layouts.admin')

@section('title')
    Index Brand
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="d-flex mb-5 justify-content-between">
                <h5 class="font-weight-bold">لیست برندها ({{ $brands->total() }})</h5>
                <a class="btn btn-outline-primary" href="{{ route('admin.brands.create') }}">
                    <i class="fa fa-plus"></i>
                    ایجاد برند
                </a>
            </div>
            <hr>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ردیف</th>
                        <th scope="col">نام برند</th>
                        <th scope="col">وضعیت</th>
                        <th scope="col">عملیات</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($brands as $key => $brand)
                        <tr>
                            <th scope="row">{{ $brands->firstItem() + $key }}</th>
                            <td>{{ $brand->name }}</td>
                            <td class="{{ $brand->getRawOriginal('is_active') ? 'text-success' : 'text-danger' }}">
                                {{ $brand->is_active }}</td>
                            <td class="col-md-auto">
                                <a class="btn btn-outline-primary"
                                    href="{{ route('admin.brands.show', ['brand' => $brand->id]) }}">
                                    <li class="fa fa-eye"></li>
                                    نمایش
                                </a>
                                <a class="btn btn-outline-success"
                                    href="{{ route('admin.brands.edit', ['brand' => $brand->id]) }}">
                                    <i class="fa fa-edit"></i>
                                    ویرایش
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $brands->links() }}
        </div>
    </div>
@endsection
