@extends('admin.layouts.admin')

@section('title')
    Show Brand
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">نمایش برند</h5>
            </div>
            <hr>

            <div class="row">
                <div class="form-group col-md-3">
                    <label>نام</label>
                    <input class="form-control" type="text" disabled value="{{ $brand->name }}">
                </div>
                <div class="form-group col-md-3">
                    <label>وضعیت</label>
                    <input class="form-control" type="text" disabled value="{{ $brand->is_active }}">
                </div>
                <div class="form-group col-md-3">
                    <label>تاریخ ایجاد</label>
                    <input class="form-control" type="text" disabled value="{{ verta($brand->created_at) }}">
                </div>
            </div>

            <a class="btn btn-dark mt-5" href="{{ route('admin.brands.index') }}"><i class="fa fa-fw fa-chevron-left"></i>
                بازگشت</a>

        </div>
    </div>
@endsection
