@extends('admin.layouts.admin')

@section('title')
    Create Category
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">ایجاد دسته بندی</h5>
            </div>
            <hr>
            @include('admin.sections.errors')
            <form action="{{ route('admin.categories.store') }}" method="post">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">نام</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="slug">نام انگلیسی</label>
                        <input class="form-control" type="text" name="slug" id="slug" value="{{ old('slug') }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="parent_id">والد</label>
                        <select class="form-control" name="parent_id" id="parent_id">
                            <option value="0">بدون والد</option>
                            @foreach ($parentCategories as $parentCategory)
                             <option value="{{ $parentCategory->id }}">{{ $parentCategory->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="is_active">وضعیت</label>
                        <select class="form-control" name="is_active" id="is_active">
                            <option value="0">غیرفعال</option>
                            <option selected value="1">فعال</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="attributeSelect">ویژگی ها</label>
                         <select id="attributeSelect" class="form-control" name="attribute_ids[]" data-actions-box="true" multiple data-live-search="true" title="انتخاب ویژگی">
                        @foreach ($attributes as $attribute)
                            <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="attributeIsFilterSelect">انتخاب ویژگی های قابل فیلتر</label>
                         <select id="attributeIsFilterSelect" class="form-control" name="attribute_is_filter_ids[]" data-actions-box="true" multiple data-live-search="true" title="انتخاب ویژگی">

                      </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="variationSelect">انتخاب ویژگی متغیر</label>
                         <select id="variationSelect" class="form-control" name="variation_id"  data-live-search="true" title="انتخاب ویژگی">

                      </select>
                    </div>
                     <div class="form-group col-md-3">
                        <label for="icon">آیکون</label>
                        <input class="form-control" type="text" name="icon" id="icon" value="{{ old('icon') }}">
                    </div>
                     <div class="form-group col-md-12">
                        <label for="description">توضیحات</label>
                        <textarea class="form-control" name="description" id="description">{!!old('description') !!}"</textarea>
                    </div>



                </div>
                <button class="btn btn-outline-primary mt-5" type="submit">ثبت</button>
                <a class="btn btn-dark mt-5 mr-3" href="{{ route('admin.brands.index') }}"><i class="fa fa-fw fa-chevron-left"></i>بازگشت</a>
            </form>

        </div>
    </div>
@endsection

@section('script')
<script>
 $('#attributeSelect') .selectpicker();
 $('#attributeIsFilterSelect') .selectpicker();
 $('#variationSelect') .selectpicker();

 $('#attributeSelect').change(function () {
      let attributesSelected = $(this).val();
            let attributes = @json($attributes);

            let attributeForFilter = [];

            attributes.map((attribute) => {
                $.each(attributesSelected , function(i,element){
                    if( attribute.id == element ){
                        attributeForFilter.push(attribute);
                    }
                });
            });

            $("#attributeIsFilterSelect").find("option").remove();
            $("#variationSelect").find("option").remove();
            attributeForFilter.forEach((element)=>{
                let attributeFilterOption = $("<option/>" , {
                    value : element.id,
                    text : element.name
                });

                let variationOption = $("<option/>" , {
                    value : element.id,
                    text : element.name
                });

                $("#attributeIsFilterSelect").append(attributeFilterOption);
                $("#attributeIsFilterSelect").selectpicker('refresh');

                $("#variationSelect").append(variationOption);
                $("#variationSelect").selectpicker('refresh');
            });
 });

</script>
@endsection
