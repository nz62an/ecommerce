@extends('admin.layouts.admin')

@section('title')
    Show Category
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">نمایش دسته بندی</h5>
            </div>
            <hr>

            <div class="row">
                <div class="form-group col-md-3">
                    <label>نام</label>
                    <input class="form-control" type="text" disabled value="{{ $category->name }}">
                </div>
                <div class="form-group col-md-3">
                    <label>نام انگلیسی</label>
                    <input class="form-control" type="text" disabled value="{{ $category->slug }}">
                </div>
                <div class="form-group col-md-3">
                    <label>والد</label>
                    <input class="form-control" type="text" disabled
                        value=" @if ($category->parent_id == 0) {{ 'بدون والد' }}
                        @else
                         {{ $category->parent->name }} @endif">
                </div>
                <div class="form-group col-md-3">
                    <label>وضعیت</label>
                    <input class="form-control" type="text" disabled value="{{ $category->is_active }}">
                </div>
                <div class="form-group col-md-3">
                    <label> ایکون</label>
                    <input class="form-control" type="text" disabled value="{{ $category->icon }}">
                </div>
                <div class="form-group col-md-3">
                    <label>تاریخ ایجاد</label>
                    <input class="form-control" type="text" disabled value="{{ verta($category->created_at) }}">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <label>ویژگی ها</label>
                    <div class="form-control disabled">
                        @foreach ($category->attributes as $attribute)
                            {{ $attribute->name }} {{ $loop->last ? '' : ',' }}
                        @endforeach
                    </div>
                </div>
                <div class="col-md-3">
                    <label>ویژگی های قابل فیلتر</label>
                    <div class="form-control disabled">
                        @foreach ($category->attributes()->wherePivot('is_filter',1)->get() as $attribute)
                            {{ $attribute->name }} {{ $loop->last ? '' : ',' }}
                        @endforeach
                    </div>
                </div>
                <div class="col-md-3">
                    <label>ویژگی متغیر</label>
                    <div class="form-control disabled">
                        @foreach ($category->attributes()->wherePivot('is_variation',1)->get() as $attribute)
                            {{ $attribute->name }} {{ $loop->last ? '' : ',' }}
                        @endforeach
                    </div>
                </div>
            </div>

            <a class="btn btn-dark mt-5" href="{{ route('admin.categories.index') }}"><i
                    class="fa fa-fw fa-chevron-left"></i>
                بازگشت</a>

        </div>
    </div>
@endsection
