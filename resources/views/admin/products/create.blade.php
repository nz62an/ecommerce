@extends('admin.layouts.admin')

@section('title')
    Create Product
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="mb-4">
                <h5 class="font-weight-bold">ایجاد محصول</h5>
            </div>
            <hr>
            @include('admin.sections.errors')
            <form action="{{ route('admin.products.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">نام</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="brandSelect">برند</label>
                        <select class="form-control" name="brandSelect" id="brandSelect" title="انتخاب برند">
                            @foreach ($brands as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="is_active">وضعیت</label>
                        <select class="form-control" name="is_active" id="is_active">
                            <option value="0">غیرفعال</option>
                            <option selected value="1">فعال</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="tagsSelect">تگ</label>
                        <select class="form-control" name="tagsSelect[]" id="tagsSelect" multiple data-actions-box="true"
                            data-live-search="true" title="انتخاب تگ">
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description">توضیحات</label>
                        <textarea class="form-control" name="description" id="description">{!! old('description') !!}"</textarea>
                    </div>

                    {{-- Product Image Section --}}
                    <div class="col-md-12">
                        <hr>
                        <p class="font-weight-bold">
                            تصاویر محصول:
                        </p>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="primary_image">انتخاب تصویر اصلی</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="primary_image" name="primary_image">
                            <label class="custom-file-label" for="primary_image">انتخاب فایل</label>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="images">انتخاب تصاویر </label>
                        <div class="custom-file">
                            <input type="file" multiple class="custom-file-input" id="images" name="images[]">
                            <label class="custom-file-label" for="images">انتخاب فایل ها</label>
                        </div>
                    </div>

                    {{-- Categories and attributes Section --}}
                    <div class="col-md-12">
                        <hr>
                        <p class="font-weight-bold">
                            دسته بندی و ویژگی ها:
                        </p>
                    </div>

                    <div class="col-md-12">
                        <div class="row justify-content-center">
                            <div class="form-group  col-md-3">
                                <label for="categorySelect">دسته بندی</label>
                                <select class="form-control" name="categorySelect" id="categorySelect"
                                    data-live-search="true" title="انتخاب دسته بندی">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}-
                                            {{ $category->parent->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="attributesContainer" class="col-md-12">
                        <div id="attributes" class="row">

                        </div>
                        <div class="col-md-12">
                            <hr>
                            <p>
                                افزودن قیمت و موجودی برای متغیر <span class="font-weight-bold" id="variationName"></span>:
                            </p>
                        </div>
                        <div id="czContainer">
                            <div id="first">
                                <div class="recordset">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label>نام</label>
                                            <input class="form-control" type="text" name="variation[value][]">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>قیمت</label>
                                            <input class="form-control" type="text" name="variation[price][]">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>تعداد</label>
                                            <input class="form-control" type="text" name="variation[quantity][]">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>شناسه انبار</label>
                                            <input class="form-control" type="text" name="variation[sku][]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     {{-- Delivery Section --}}
                     <div class="col-md-12">
                        <hr>
                        <p class="font-weight-bold">
                            هزینه ارسال:
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="delivery_amount">هزینه ارسال</label>
                        <input class="form-control" type="text" name="delivery_amount" id="delivery_amount" value="{{ old('delivery_amount')}}">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="delivery_amount_per_product">هزینه ارسال به ازای محصول اضافی</label>
                        <input class="form-control" type="text" name="delivery_amount_per_product" id="delivery_amount_per_product" value="{{ old('delivery_amount_per_product')}}">
                    </div>


                </div>


                <!-- The elements you want repeated must be wrapped in an element with id="recordset" -->

                {{-- <div class="form-group col-md-3">
                        <label for="attributeSelect">ویژگی ها</label>
                        <select id="attributeSelect" class="form-control" name="attribute_ids[]" data-actions-box="true"
                            multiple data-live-search="true" title="انتخاب ویژگی">
                            @foreach ($attributes as $attribute)
                                <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="attributeIsFilterSelect">انتخاب ویژگی های قابل فیلتر</label>
                        <select id="attributeIsFilterSelect" class="form-control" name="attribute_is_filter_ids[]"
                            data-actions-box="true" multiple data-live-search="true" title="انتخاب ویژگی">

                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="variationSelect">انتخاب ویژگی متغیر</label>
                        <select id="variationSelect" class="form-control" name="variation_id" data-live-search="true"
                            title="انتخاب ویژگی">

                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="icon">آیکون</label>
                        <input class="form-control" type="text" name="icon" id="icon" value="{{ old('icon') }}">
                    </div> --}}



                <button class="btn btn-outline-primary mt-5" type="submit">ثبت</button>
                <a class="btn btn-dark mt-5 mr-3" href="{{ route('admin.products.index') }}"><i
                        class="fa fa-fw fa-chevron-left"></i>بازگشت</a>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#tagsSelect').selectpicker();
        $('#brandSelect').selectpicker();
        $('#primary_image').change(function() {

            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName);
        });
        $('#images').change(function() {

            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName);
        });
        $('#categorySelect').selectpicker();
        $('#attributesContainer').hide();
        $('#categorySelect').change(function() {

            let categorySelected = $(this).val();
            axios.get(`{{ url('/admin-panel/management/category-attributes/${categorySelected}') }}`)
                .then(function(response) {

                    $('#attributesContainer').fadeIn();
                    $('#attributes').empty();

                    response.data.attributes.forEach((element) => {
                        let attributeElement = $('<div/>', {
                            class: 'form-group col-md-3'
                        }).append($('<label/>', {
                            for: `attributes[${element.id}]`,
                            text: element.name
                        })).append($('<input/>', {
                            class: 'form-control',
                            type: 'text',
                            id: `attributes[${element.id}]`,
                            name: `attributes[${element.id}]`
                        }));

                        $("#attributes").append(attributeElement);

                    });

                    $('#variationName').text(response.data.variation.name);


                    console.log(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                });

        });
        $("#czContainer").czMore();
    </script>
@endsection
