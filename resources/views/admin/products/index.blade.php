@extends('admin.layouts.admin')

@section('title')
    Index Category
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="d-flex mb-5 justify-content-between">
                <h5 class="font-weight-bold">لیست دسته بندی ها ({{ $categories->total() }})</h5>
                <a class="btn btn-outline-primary" href="{{ route('admin.categories.create') }}">
                    <i class="fa fa-plus"></i>
                    ایجاد دسته بندی
                </a>
            </div>
           <div>
            <hr>
           </div>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">نام</th>
                        <th scope="col">نام انگلیسی</th>
                        <th scope="col">والد</th>
                        <th scope="col">وضعیت</th>
                        <th scope="col">عملیات</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($categories as $key => $category)
                        <tr>
                            <th scope="row">{{ $categories->firstItem() + $key }}</th>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->slug }}</td>
                            <td>
                                @if ($category->parent_id == 0)
                                    {{ 'بدون والد' }}
                                @else
                                    {{ $category->parent->name }}
                                @endif
                            </td>
                            <td class="{{ $category->getRawOriginal('is_active') ? 'text-success' : 'text-danger' }}">
                                {{ $category->is_active }}</td>
                            <td class="col-md-auto">
                                <a class="btn btn-outline-primary"
                                    href="{{ route('admin.categories.show', ['category' => $category->id]) }}">
                                    <li class="fa fa-eye"></li>
                                    نمایش
                                </a>
                                <a class="btn btn-outline-success"
                                    href="{{ route('admin.categories.edit', ['category' => $category->id]) }}">
                                    <i class="fa fa-edit"></i>
                                    ویرایش
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $categories->links() }}
        </div>
    </div>
@endsection
