@extends('admin.layouts.admin')

@section('title')
 Create Tag
@endsection

@section('content')

<div class="row">
    <div class="col mb-4 p-md-5 bg-white">
        <div class="mb-4">
            <h5 class="font-weight-bold">ایجاد تگ</h5>
        </div>
        <hr>
        @include('admin.sections.errors')
        <form action="{{ route('admin.tags.store') }}" method="post">
            @csrf
           <div class="form-row">
            <div class="form-group col-md-3">
                <label for="name">نام</label>
                <input class="form-control" type="text" name="name" id="name">
            </div>
           </div>
           <button class="btn btn-outline-primary mt-5" type="submit"><i class="fa fa-fw fa-save"></i> ثبت</button>
           <a class="btn btn-dark mt-5 mr-3" href="{{ route('admin.tags.index') }}"><i class="fa fa-fw fa-chevron-left"></i>بازگشت</a>
        </form>

    </div>
</div>
@endsection
