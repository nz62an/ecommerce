@extends('admin.layouts.admin')

@section('title')
    Index Tag
@endsection

@section('content')
    <div class="row">
        <div class="col mb-4 p-md-5 bg-white">
            <div class="d-flex mb-5 justify-content-between">
                <h5 class="font-weight-bold">لیست تگ ها ({{ $tags->total() }})</h5>
                <a class="btn btn-outline-primary" href="{{ route('admin.tags.create') }}">
                    <i class="fa fa-plus"></i>
                    ایجاد تگ
                </a>
            </div>
            <hr>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ردیف</th>
                        <th scope="col">نام تگ</th>
                        <th scope="col">عملیات</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($tags as $key => $tag)
                        <tr>
                            <th scope="row">{{ $tags->firstItem() + $key }}</th>
                            <td>{{ $tag->name }}</td>
                            <td class="col-md-auto">
                                <a class="btn btn-outline-primary"
                                    href="{{ route('admin.tags.show', ['tag' => $tag->id]) }}">
                                    <li class="fa fa-eye"></li>
                                    نمایش
                                </a>
                                <a class="btn btn-outline-success"
                                    href="{{ route('admin.tags.edit', ['tag' => $tag->id]) }}">
                                    <i class="fa fa-edit"></i>
                                    ویرایش
                                </a>
                                <form action="{{ route('admin.tags.destroy', ['tag' => $tag->id]) }}" method="post"
                                    class="d-inline-block">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger" type="submit"> <i class="fa fa-fw fa-recycle "></i>حذف</button>

                                </form>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $tags->links() }}
        </div>
    </div>
@endsection
